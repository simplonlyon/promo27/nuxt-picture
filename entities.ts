export interface User {
    id?:number;
    email:string;
    displayName:string;
    password?:string;
    role?:string;
}

export interface Picture {
    id?:number;
    title:string;
    image:string;
    description:string;
    createdAt?: string;
    author?:User;
    imageLink?:string;
    thumbnailLink?:string;
    likes?:User[];
}

export interface Comment {
    id?:number;
    content:string;
    author?:User;
    picture?:Picture;
    createdAt?:string;
}

export interface Page<T> {
    content: T[];
    totalPages:number;
    totalElements:number;
    last:boolean;
    first:boolean;
}