# Nuxt Picture

Projet Nuxt.js connecté au backend [spring-orm](https://gitlab.com/simplonlyon/promo27/spring-orm) pour aborder le SSR

## Techno
* **Nuxt.js** framework Vue.js permettant le Server Side Rendering : la pré-compilation des pages côté serveur pour améliorer la vitesse et le référencement de l'appli
* **Bootstrap** Framework CSS, juste le style chargé

## Particularité de Nuxt

### Le Router
Le router de Nuxt repose sur le nom des fichiers, on aura une séparation entre pages et components (comme dans VueJs classique), chaque fichier existant dans le dossier pages sera considéré comme une route accessible et se basera sur le nom.

Exemple : 
Un fichier `pages/salut.vue` rendra disponible la page http://localhost:3000/salut

Un fichier `pages/index.vue` rendra disponible la page http://localhost:3000

Un fichier `pages/truc/all.vue` sera disponible sur http://localhost:3000/truc/all

Pour créer des routes parametrées, on devra nommer nos fichier avec des crochets. Par exemple un fichier `pages/truc/[id].vue` sera disponible sur http://localhost:3000/truc/1 ou 2 ou 3, et la valeur du paramètre sera accessible dans la page via le useRouter dans router.params.id


### Le SSR avec useFetch
Pour permettre de pré-rendre les pages les pages, Nuxt donne accès à une fonction useFetch pour faire les requêtes backend, il prendra la place de Axios.

Le code suivant fera une requête vers le backend pour aller chercher des données et les rendre accessibles dans le front (il y a également une mise en cache des données effectuée pour augmenter les performances du site)
```ts
const {data} = await useFetch<Truc[]>('http://localhost:8080/api/truc'); 
```

La requête précédente sera exécutée côté "serveur" (le serveur de Nuxt, pas le Spring), dans les cas où on souhaiterait exécuter la requête côté client on rajoutera un `immediate: false`

Dans ce cas là, la requête ne sera exécutée que lorsque la fonction execute sera lancée (au click sur un bouton ou autre)
```ts
const {data, execute} = await useFetch<Truc[]>('http://localhost:8080/api/truc', {immediate: false}); 
```

### Les requêtes de modification avec $fetch
Le SSR ne concerne généralement que les requêtes GET (et souvent plutôt les requêtes GET sur des routes publiques). Dans le cas où on souhaite faire des POST/PUT/DELETE Nuxt rend accessible la fonction $fetch (tiré de la library ofetch)

Exemple d'un POST classique
```ts
const truc = await $fetch('http://localhost:8080/api/truc', {
    method: 'POST',
    body: myData
});
```

### Partie authentification

Pour la requête de login, dans le cas d'une authentification en HTTP Basic, il est nécessaire d'ajouter les paramètres suivant à la requête

```ts
await $fetch<User>('http://localhost:8080/api/account', {
    credentials:'include',
    headers: {
      'Authorization': 'Basic '+ btoa(email.value+':'+password.value)
    }
  });
```

On peut stocker le user connecté dans un cookie disponible en front et back de nuxt et qui sera conservé lors d'un rechargement de page. Pour ça, on utilise le useCookie.

```ts
const user = useCookie<User|null>('user');

//...

user.value = myUser;
```

Ce useCookie est réactif et fait qu'il n'est pas nécessaire d'utiliser un store pinia ou équivalent.

Pour une facilité d'utilisation, on peut créer un composable qui définira le type et le nom du cookie globalement 

Exemple dans un fichier composables/useAuth.ts
```ts
export const useAuth = () => {
  return useCookie<User|null>('user', {
    sameSite: 'strict'
  });
}
```
(Le `sameSite: 'strict'` sert à configurer le cookie pour faire que celui ci ne soit pas envoyé vers d'autres sites lors de requêtes)

On pourra alors utilisation dans les components :
```ts
const user = useAuth();
```


Pour les requêtes nécessitant d'être authentifié⋅e, on rajoutera les paramètres suivant au $fetch ou au useFetch pour envoyer les cookies de session avec la requête
```ts
await $fetch('http://localhost:8080/api/truc', {
    method: 'POST',
    body: truc,
    credentials:'include',
    headers: useRequestHeaders(['cookie'])
});
```

Si on souhaite une configuration globale de useFetch ou $fetch, on peut utiliser un composable comme pour le cookie

Exemple dans un fichier composables/fetch.ts
```ts

export function useApi<T>(
    url: string | (() => string),
    options: UseFetchOptions<T> = {}
) {
    return useFetch(url, {
        ...options,
        baseURL: 'http://localhost:8080',
        credentials: 'include',
        headers: useRequestHeaders(['cookie'])
    });
}

export const $api = $fetch.create({
    baseURL: 'http://localhost:8080',
    credentials: 'include',
    onRequest(context) {
        context.options.headers = {
            ...context.options.headers,
            'X-Requested-With': 'XMLHttpRequest',
            ...useRequestHeaders(['cookie'])
        }
    },
    onResponseError(context) {
        const route = useRoute();
        if(context.response.status == 401) {
            
            useAuth().value = null;
            navigateTo({
                path:'/login',
                query: {initialPage: route.path}
            });
        }
    }
    
})
```

On utilisera alors useApi à la place de useFetch et $api à la place de $fetch (même utilisation, mais pas obligé de répété les headers/cookies ou l'url de base du server)

Ici, le `$api` a également une configuration pour déconnecter et rediriger vers la page login lorsqu'un appel serveur renvoie une erreur 401 (le onResponseError pourrait être également assigné dans le useApi)